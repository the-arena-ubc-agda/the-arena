﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MonsterGorlAtStairs : MonoBehaviour {

    private GameObject girlSpriteGameobject;

    private void Start()
    {
        girlSpriteGameobject = GameObject.FindGameObjectWithTag("Sprite/Girl");
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.name == "Girl")
        {
            StartCoroutine("MonsterEvent");
        }
    }

    private IEnumerator MonsterEvent()
    {
        GetComponent<Collider>().enabled = false;

        EventManager.TriggerEvent("StopMoving");
        CameraShake cameraShake = GameObject.FindWithTag("CameraHolder").GetComponent<CameraShake>();
        StartCoroutine(cameraShake.LaserShake(4f, .15f));

        yield return new WaitForSeconds(2f);

        AudioSource audio = gameObject.AddComponent<AudioSource>();
        audio.volume = 0.05f;
        audio.PlayOneShot(Resources.Load("BackgroundMusic/troll_monster_growl_long_06") as AudioClip);
        Destroy(GetComponent<AudioSource>(), 5f);

        SoundManager.ExitMusic1();
        yield return new WaitForSeconds(2f);

        SoundManager.SetBackgroundMusic1(Resources.Load("BackgroundMusic/DungeonFloor1-CrystallRoom") as AudioClip,true , 0.03f, 0.05f,0.05f);
        SoundManager.StartCountingForSeemlessTransition1to2();
        SoundManager.EnterMusic1();


        girlSpriteGameobject.GetComponent<SpriteScript2>().ForcePlayerRotation(4);

        //TODO Try to add the text either here or before the previous yield. Textfilename : "DungeonGrowl"
        GetComponent<TextGrowl>().StartInteraction();

        yield return new WaitForSeconds(1f);
        girlSpriteGameobject.GetComponent<SpriteScript2>().ForcePlayerRotation(3);
        Destroy(this.gameObject);
    }

    private void OnDestroy()
    {
        EventManager.TriggerEvent("StartMoving");
    }
}
