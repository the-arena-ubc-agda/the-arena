﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SoundManager : MonoBehaviour {

    private static SoundManager soundManager;

    public static int BackgroundMusicVolume = 70;
    public static int SFXVolume = 70;
    public Slider BackgroundMusicSlider;
    public Slider SFXVolumeSlider;
    public Text backgroundMusicSliderText;
    public Text sfxVolumeText;

    public delegate void OnSoundChanged();
    public static OnSoundChanged onSoundChangedCallback;

    private bool exitMusic1 = false;
    private bool enterMusic1 = false;
    private bool exitMusic2 = false;
    private bool enterMusic2 = false;
    private bool exitSFX = false;
    private bool enterSFX = false;

    [SerializeField] private float scaleFactor1 = 0.65f;
    [SerializeField] private float exitSpeed1 = 0.1f;
    private float time1 = 0;
    [SerializeField] private float scaleFactor2 = 0.65f;
    [SerializeField] private float exitSpeed2 = 0.1f;
    private float time2 = 0;

    [SerializeField] private float scaleFactorSFX = 0.65f;
    [SerializeField] private float exitSpeedSFX = 0.1f;
    private float backgroundSFXTime = 0;

    private double clip1Length;
    private double clip2Length;
    private double sfxLength;

    private double seemlessTransitionTimer;

    private bool seemlessTransition1to2;
    private bool seemlessTransition1to2Start;
    private string seemlessTransitionAudioName;

    private bool seemlessTransition2to1;
    private bool seemlessTransition2to1Start;

    public static SoundManager Instance
    {
        get
        {
            if (!soundManager)
            {
                soundManager = FindObjectOfType(typeof(SoundManager)) as SoundManager;

                if (!soundManager)
                {
                    Debug.LogError("There needs to be one active EventManger script on a GameObject in your scene.");
                }
                else
                {
                    soundManager.Init();
                }
            }
            return soundManager;
        }
    }

    void Init()
    {
    }

    private void Start()
    {
        if (onSoundChangedCallback != null)
        {
            onSoundChangedCallback.Invoke();
        }
    }

    private void Update()
    {
        if (BackgroundMusicSlider.isActiveAndEnabled)
        {
            AudioSource[] backgroundMusicSource = gameObject.GetComponentsInChildren<AudioSource>();

            BackgroundMusicVolume = (int) BackgroundMusicSlider.value;
            backgroundMusicSliderText.text = BackgroundMusicSlider.value.ToString();

            backgroundMusicSource[0].volume = (BackgroundMusicVolume * scaleFactor1) / 100;
            backgroundMusicSource[1].volume = (BackgroundMusicVolume * scaleFactor2) / 100;

            if (onSoundChangedCallback != null)
            {
                onSoundChangedCallback.Invoke();
            }
        }

        //Debug.Log(SFXVolumeSlider.isActiveAndEnabled);

        if (SFXVolumeSlider.isActiveAndEnabled)
        {
            SFXVolume = (int)SFXVolumeSlider.value;
            sfxVolumeText.text = SFXVolumeSlider.value.ToString();

            if (onSoundChangedCallback != null)
            {
                onSoundChangedCallback.Invoke();
            }
        }

        if(exitMusic1)
        {
            AudioSource[] backgroundMusicSource = gameObject.GetComponentsInChildren<AudioSource>();

            if (time1 < 1)
            {             
                backgroundMusicSource[0].volume = Mathf.Lerp(backgroundMusicSource[0].volume, 0, time1);
                time1 += Time.deltaTime * exitSpeed1;
            }
            if(backgroundMusicSource[0].volume <= Mathf.Pow(10, -6))
            {
                backgroundMusicSource[0].Stop();
                time1 = 0;
                exitMusic1 = false;
            }
        }
        else if (enterMusic1)
        {
            AudioSource[] backgroundMusicSource = gameObject.GetComponentsInChildren<AudioSource>();

            if (time1 < 1)
            {
                backgroundMusicSource[0].volume = Mathf.Lerp(0, scaleFactor1, time1);           
                time1 += Time.deltaTime * exitSpeed1;
            }
            else
            {
                enterMusic1 = false;
                time1 = 0;
            }

            if (backgroundMusicSource[0].isPlaying == false)
            {
                backgroundMusicSource[0].Play();
            }
        }

        if (exitMusic2)
        {
            AudioSource[] backgroundMusicSource = gameObject.GetComponentsInChildren<AudioSource>();

            if (time1 < 1)
            {
                backgroundMusicSource[1].volume = Mathf.Lerp(backgroundMusicSource[1].volume, 0, time2);
                time2 += Time.deltaTime * exitSpeed2;
            }
            if (backgroundMusicSource[1].volume <= Mathf.Pow(10, -6))
            {
                backgroundMusicSource[1].Stop();
                time2 = 0;
                exitMusic2 = false;
            }
        }
        else if (enterMusic2)
        {
            AudioSource[] backgroundMusicSource = gameObject.GetComponentsInChildren<AudioSource>();

            if (time2 < 1)
            {
                backgroundMusicSource[1].volume = Mathf.Lerp(0, scaleFactor2, time2);
                time2 += Time.deltaTime * exitSpeed2;
            }
            else
            {
                enterMusic2 = false;
                time1 = 0;
            }

            if (backgroundMusicSource[1].isPlaying == false)
            {
                backgroundMusicSource[1].Play();
            }
        }

        if (exitSFX)
        {
            AudioSource[] backgroundSFXSource = gameObject.GetComponentsInChildren<AudioSource>();

            if (backgroundSFXTime < 1)
            {
                backgroundSFXSource[2].volume = Mathf.Lerp(backgroundSFXSource[2].volume, 0, backgroundSFXTime);
                backgroundSFXTime += Time.deltaTime * exitSpeedSFX * 0.1f;
            }
            if (backgroundSFXSource[1].volume <= Mathf.Pow(10, -6))
            {
                backgroundSFXSource[1].Stop();
                backgroundSFXTime = 0;
                exitSFX = false;
            }
        }
        else if (enterSFX)
        {
            AudioSource[] backgroundSFXSource = gameObject.GetComponentsInChildren<AudioSource>();

            if (backgroundSFXTime < 1)
            {
                backgroundSFXSource[2].volume = Mathf.Lerp(0, 0.005f, backgroundSFXTime);
                backgroundSFXTime += Time.deltaTime * exitSpeedSFX;
            }
            else
            {
                enterSFX = false;
                backgroundSFXTime = 0;
            }

            if (backgroundSFXSource[1].isPlaying == false)
            {
                backgroundSFXSource[1].Play();
            }
        }

        if (seemlessTransition1to2)
        {
            if (seemlessTransitionTimer > 0)
            {
                seemlessTransitionTimer -= Time.deltaTime;
            }
            else
            {
                if (seemlessTransition1to2Start)
                {
                    AudioSource[] backgroundMusicSource = gameObject.GetComponentsInChildren<AudioSource>();
                    backgroundMusicSource[0].Stop();
                    backgroundMusicSource[1].Play();
                    seemlessTransition1to2Start = false;
                    seemlessTransition1to2 = false;
                }
                seemlessTransitionTimer = clip1Length;
            }
        }
        if (seemlessTransition2to1)
        {
            if (seemlessTransitionTimer > 0)
            {
                seemlessTransitionTimer -= Time.deltaTime;
            }
            else
            {
                if (seemlessTransition2to1Start)
                {
                    AudioSource[] backgroundMusicSource = gameObject.GetComponentsInChildren<AudioSource>();
                    backgroundMusicSource[1].Stop();
                    seemlessTransition2to1Start = false;
                    seemlessTransition2to1 = false;
                }
                seemlessTransitionTimer = clip2Length;
            }
        }
    }

    public static void SetBackgroundMusic1(AudioClip music, bool loop = true, float scaleMusic = 0.65f, float enterScene = 0.1f, float exitScene = 0.1f)
    {
        AudioSource[] backgroundMusicSource = Instance.GetComponentsInChildren<AudioSource>();
        backgroundMusicSource[0].clip = music;
        backgroundMusicSource[0].loop = loop;
        Instance.scaleFactor1 = scaleMusic;
        Instance.exitSpeed1 = exitScene;
    }
    public static void SetBackgroundMusic2(AudioClip music, bool loop = true, float scaleMusic = 0.65f, float enterScene = 0.1f, float exitScene = 0.1f)
    {
        AudioSource[] backgroundMusicSource = Instance.GetComponentsInChildren<AudioSource>();
        backgroundMusicSource[1].clip = music;
        backgroundMusicSource[1].loop = loop;
        Instance.scaleFactor2 = enterScene;
        Instance.exitSpeed2 = exitScene;
    }
    public static void SetBackgroundSFX(AudioClip SFX)
    {
        AudioSource[] backgroundSFXSource = Instance.GetComponentsInChildren<AudioSource>();
        backgroundSFXSource[2].clip = SFX;
    }

    public static void ExitMusic1()
    {
        Instance.exitMusic1 = true;
        Instance.enterMusic1 = false;
        Instance.time1 = 0;
    }
    public static void EnterMusic1()
    {
        Instance.exitMusic1 = false;
        Instance.enterMusic1 = true;
        Instance.time1 = 0;
    }

    public static void ExitMusic2()
    {
        Instance.exitMusic2 = true;
        Instance.enterMusic2 = false;
        Instance.time2 = 0;
    }
    public static void EnterMusic2()
    {
        Instance.exitMusic2 = false;
        Instance.enterMusic2 = true;
        Instance.time2 = 0;
    }

    public static void FadeOutBackgroundSFX()
    {
        AudioSource[] backgroundSFXSource = Instance.GetComponentsInChildren<AudioSource>();
        Instance.exitSFX = false;
        Instance.enterSFX = true;
        Instance.backgroundSFXTime = 0;
    }
    public static void FadeInBackgroundSFX()
    {
        AudioSource[] backgroundSFXSource = Instance.GetComponentsInChildren<AudioSource>();
        Instance.enterSFX = true;
        Instance.exitSFX = false;
        Instance.backgroundSFXTime = 0;
    }

    public static void StartCountingForSeemlessTransition1to2()
    {
        AudioSource[] backgroundMusicSource = Instance.GetComponentsInChildren<AudioSource>();
        Instance.clip1Length = backgroundMusicSource[0].clip.samples/ backgroundMusicSource[0].clip.frequency;
        Instance.seemlessTransitionTimer = Instance.clip1Length;
        Instance.seemlessTransition1to2 = true;
    }
    public static void StartCountingForSeemlessTransition2to1()
    {
        AudioSource[] backgroundMusicSource = Instance.GetComponentsInChildren<AudioSource>();
        Instance.clip2Length = backgroundMusicSource[1].clip.samples / backgroundMusicSource[1].clip.frequency;
        Instance.seemlessTransitionTimer = Instance.clip2Length;
        Instance.seemlessTransition2to1 = true;
    }

    public static void SetSeemlessTransitionFrom1to2(string audioname, float volume = 0)
    {
        Debug.Log("We should switch music from AudioSource 1 to 2 in the next iteration");

        AudioSource[] backgroundSFXSource = Instance.GetComponentsInChildren<AudioSource>();
        backgroundSFXSource[1].clip = Resources.Load(audioname) as AudioClip;
        if(volume != 0)
            backgroundSFXSource[1].volume = volume;
        else 
            backgroundSFXSource[1].volume = backgroundSFXSource[0].volume;        
        Instance.seemlessTransition1to2Start = true;
    }
    public static void SetSeemlessTransitionFrom2to1(string audioname)
    {
        Debug.Log("We should switch music from AudioSource 2 to 1 in the next iteration");

        AudioSource[] backgroundSFXSource = Instance.GetComponentsInChildren<AudioSource>();
        backgroundSFXSource[0].clip = Resources.Load(audioname) as AudioClip;
        backgroundSFXSource[0].volume = backgroundSFXSource[1].volume;

        Instance.seemlessTransition2to1Start = true;
    }

}
